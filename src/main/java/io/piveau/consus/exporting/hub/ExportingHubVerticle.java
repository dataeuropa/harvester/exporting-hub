package io.piveau.consus.exporting.hub;

import io.piveau.pipe.PipeContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.pointer.JsonPointer;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

import java.util.Set;
import java.util.stream.Collectors;

public class ExportingHubVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.exporting.hub.queue";

    public static final String API_KEY_HEADER = "X-API-Key";

    public static final String PATH_CATALOGUES = "catalogues";

    private static final JsonPointer addressPointer = JsonPointer.from("/hub/endpoint/address");
    private static final JsonPointer apiKeyPointer = JsonPointer.from("/hub/endpoint/apiKey");

    private WebClient client;

    private String hubAddress;
    private String hubApiKey;

    private CircuitBreaker circuitBreaker;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx, new WebClientOptions().setKeepAlive(true).setMaxPoolSize(200));

        circuitBreaker = CircuitBreaker
                .create("exporting-hub", vertx, new CircuitBreakerOptions().setMaxRetries(3).setTimeout(-1))
                .retryPolicy((t, c) -> c * 1000L);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add("PIVEAU_HUB_ADDRESS")
                        .add("PIVEAU_HUB_APIKEY")));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig(ar -> {
            if (ar.succeeded()) {
                hubAddress = ar.result().getString("PIVEAU_HUB_ADDRESS", "http://piveau-hub-repo:8080");
                hubApiKey = ar.result().getString("PIVEAU_HUB_APIKEY", "apiKey");
                startPromise.complete();
            } else {
                startPromise.fail(ar.cause());
            }
        });
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();

        if (pipeContext.log().isDebugEnabled()) {
            pipeContext.log().debug(pipeContext.getPipeManager().prettyPrint());
        }

        JsonObject dataInfo = pipeContext.getDataInfo();

        JsonObject config = pipeContext.getConfig();

        String address = (String) addressPointer.queryJsonOrDefault(config, hubAddress);
        String apiKey = (String) apiKeyPointer.queryJsonOrDefault(config, hubApiKey);

        switch (dataInfo.getString("content", "")) {
            case "identifierList" -> deleteIdentifiers(address, apiKey, pipeContext);
            case "metrics" -> exportMetrics(address, apiKey, pipeContext);
            default -> exportMetadata(address, apiKey, pipeContext);
        }
    }

    private void exportMetadata(String address, String apiKey, PipeContext pipeContext) {

        JsonObject dataInfo = pipeContext.getDataInfo();

        String catalogueId = dataInfo.getString("catalogue", "");
        String originalId = dataInfo.getString("identifier", "");

        circuitBreaker.<HttpResponse<Buffer>>execute(promise -> client.putAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets/origin")
                        .putHeader(API_KEY_HEADER, apiKey)
                        .putHeader(HttpHeaders.CONTENT_TYPE.toString(), pipeContext.getMimeType())
                        .addQueryParam("originalId", originalId)
                        .timeout(360000)
                        .sendBuffer(Buffer.buffer(pipeContext.getStringData()))
                        .onComplete(promise)
                )
                .onSuccess(response -> {
                    switch (response.statusCode()) {
                        case 200, 204 -> pipeContext.log().info("Dataset updated: {}", dataInfo);
                        case 201 -> pipeContext.log().info("Dataset created: {}", dataInfo);
                        case 304 -> pipeContext.log().info("Dataset skipped: {}", dataInfo);
                        case 400 ->
                                pipeContext.log().error("Dataset failure: {} - {}", dataInfo, response.body().toString());
                        default ->
                                pipeContext.log().error("Dataset failure: {}", dataInfo, new Throwable("" + response.statusCode() + " - " + response.statusMessage() + ": " + response.bodyAsString()));
                    }
                })
                .onFailure(pipeContext::setFailure);

        pipeContext.getPipeManager().freeData();
    }

    private void exportMetrics(String address, String apiKey, PipeContext pipeContext) {

        JsonObject dataInfo = pipeContext.getDataInfo();

        String datasetId = dataInfo.getString("identifier", "");
        circuitBreaker.<HttpResponse<Buffer>>execute(promise -> client.putAbs(address + "/datasets/" + datasetId + "/metrics")
                        .putHeader(API_KEY_HEADER, apiKey)
                        .putHeader(HttpHeaders.CONTENT_TYPE.toString(), pipeContext.getMimeType())
                        .timeout(360000)
                        .sendBuffer(Buffer.buffer(pipeContext.getStringData()))
                        .onComplete(promise))
                .onSuccess(response -> {
                    switch (response.statusCode()) {
                        case 200, 204 -> pipeContext.log().info("Metrics graph updated: {}", dataInfo);
                        case 201 -> pipeContext.log().info("Metrics graph created: {}", dataInfo);
                        default ->
                                pipeContext.setFailure(new Throwable(dataInfo.getString("id"), new Throwable("" + response.statusCode() + " - " + response.statusMessage() + " - " + response.bodyAsString())));
                    }
                })
                .onFailure(pipeContext::setFailure);

        pipeContext.getPipeManager().freeData();
    }


    private void deleteIdentifiers(String address, String apiKey, PipeContext pipeContext) {

        String catalogueId = pipeContext.getDataInfo().getString("catalogue", "");
        circuitBreaker.<HttpResponse<Buffer>>execute(promise -> client.getAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets")
                        .putHeader(API_KEY_HEADER, apiKey)
                        .addQueryParam("valueType", "originalIds")
                        .timeout(360000)
                        .send()
                        .onComplete(promise)
                )
                .onSuccess(response -> {
                    if (response.statusCode() == 200) {
                        Set<String> sourceIds = new JsonArray(pipeContext.getStringData()).stream().map(Object::toString).collect(Collectors.toSet());
                        pipeContext.getPipeManager().freeData();
                        Set<String> targetIds = response.bodyAsJsonArray().stream().map(Object::toString).collect(Collectors.toSet());
                        int targetSize = targetIds.size();
                        targetIds.removeAll(sourceIds);
                        pipeContext.log().info("Source {}, target {}, deleting {} datasets", sourceIds.size(), targetSize, targetIds.size());
                        targetIds.forEach(datasetId -> deleteDataset(address, apiKey, pipeContext, datasetId, catalogueId));
                    } else {
                        pipeContext.setFailure(response.statusMessage());
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

    private void deleteDataset(String address, String apiKey, PipeContext pipeContext, String datasetId, String catalogueId) {
        circuitBreaker.<HttpResponse<Buffer>>execute(promise -> client.deleteAbs(address + "/" + PATH_CATALOGUES + "/" + catalogueId + "/datasets/origin")
                        .putHeader(API_KEY_HEADER, apiKey)
                        .addQueryParam("originalId", datasetId)
                        .timeout(360000)
                        .send()
                        .onComplete(promise)
                )
                .onSuccess(response -> {
                    switch (response.statusCode()) {
                        case 200, 202, 204 -> pipeContext.log().info("Dataset deleted: {}", datasetId);
                        case 404 -> pipeContext.log().warn("Dataset not found: {}", datasetId);
                        default ->
                                pipeContext.log().error("{} - {} ({})", response.statusCode(), response.statusMessage(), datasetId);
                    }
                })
                .onFailure(cause -> pipeContext.log().error("Delete dataset", cause));
    }

}
