# ChangeLog

## 4.0.6 (2023-11-07)

**Changed:**
* Dependencies update

## 4.0.5 (2023-09-22)

**Changed:**
* README.md update

## 4.0.4 (2023-08-25)

**Added:**
* Extra merge strategy for CHANGELOG.md (re-added)

## 4.0.3 (2023-08-25)

**Removed:**
* Extra merge strategy for CHANGELOG.md 

## 4.0.2 (2023-07-20)

**Changed:**
* Another bump up of dependencies

## 4.0.1 (2023-02-01)

**Fix:**
* Bump up dependencies

## 4.0.0 (2023-02-01)

**Changed:**
* Use of metrics API
* Updated all repo API usages

## 3.1.12 (2022-11-25)

**Added:**
* Timeout for put requests (5min)

## 3.1.11 (2022-06-28)

**Fixed:**
* Connector and body handler

## 3.1.10 (2022-06-27)

**Changed:**
* Lib updates

## 3.1.9 (2022-04-24)

**Changed:**
* Log output of 'deleted' response for better filtering in kibana

## 3.1.8 (2022-04-20)

**Changed:**
* Lib updates

## 3.1.7 (2022-02-12)

**Changed:**
* Set default api key to `anyKey`

## 3.1.6 (2021-10-20)

**Changed:**
* Important connector lib update
* Increased web client pool size
* Using event loop instead of worker verticle

## 3.1.5 (2021-06-23)

**Changed:**
* Connector pipe handling

## 3.1.4 (2021-06-05)

**Changed:**
* Lib dependencies

## 3.1.3 (2021-03-19)

**Changed:**
* CI/CD

## 3.1.2 (2021-03-19)

**Fixed:**
* Vert.x issues (updated to 4.0.3)

## 3.1.1 (2021-01-29)

Hotfix!

**Fixed:**
* Null content type in dataInfo

## 3.1.0 (2021-01-27)

**Changed:**
* Switched to Vert.x 4.0.0
* Passing to next segment removed. Consider it more an end segment for now

## 3.0.1 (2020-11-17)

**Fixed:**
* Accept also 204 responses

## 3.0.0 (2020-11-12)

**Changed:**
* Response failures logging
* Passing datasets in any case 

**Removed:**
* Hash functionality

## 2.0.1 (2020-03-11)

Many minor fixes and lib updates!

## 2.0.0 (2020-03-11)

**Added:**
* Circuit breaker for exporting metrics

**Changed:**
* Use uriRef for exporting metrics

## 1.1.2 (2020-03-05)

**Changed:**
* Lib updates

## 1.1.1 (2020-02-28)

**Changed:**
* Use JsonObject for dataInfo
* Pass after hub transaction finished 

## 1.1.0 (2019-12-18)

**Changed:**
* Pass id as query param in put and delete dataset or metrics

## 1.0.0 (2019-11-08)

**Added:**
* `PIVEAU_LOG_LEVEL` for general log level configuration of the `io.piveau` package
* `PIVEAU_HUB_ADDRESS`, `PIVEAU_HUB_APIKEY` in case of missing endpoint in pipe segment config
* `PIVEAU_HUB_ADD_HASH` to add canonical hash if no hash is present
* Ability to export only the Datasets Metrics to the Hub

**Changed:**
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Fixed:**
* Update all dependencies

## 0.1.2 (2019-06-21)

**Added:**
* buildInfo.json for build info via `/health` path
* config.schema.json

**Changed:**
* `PIVEAU_` prefix to logstash configuration environment variables

**Fixed:**
* encode identifier when delete

## 0.1.1 (2019-05-17)

**Fixed:**
* Hub address structure reading in delete

## 0.1.0 (2019-05-17)

**Changed:**
* Configuration structure of pipe segment configuration
* Readme

## 0.0.1 (2019-05-03)

Initial release
